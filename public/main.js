'use strict';

// Wrap everything in an anonymous function to avoid polluting the global namespace
(function () {

  const worksheetSettingsKey = 'selectedWorksheet';
  const datasourcesSettingsKey = 'selectedDatasource';
  const fieldSettingsKey = 'selectedField';
  const urlKey = 'url';

  //  Define the defaults for various links
  const links ={
    'configDialog': 'https://tableau-extensions.gitlab.io/dynamiciframe/configDialog.html',
    'notFound': './resources/not-found.png'
  }

  // Maps dataSource id to dataSource so we can keep track of unique dataSources.
  let dashboardDataSources = {};

  $(document).ready(function () {

    //  Initialize the extension, and include a configure button in the settings menu
    tableau.extensions.initializeAsync({'configure': configure}).then(function() {

      // To get dataSource info, first get the dashboard.
      const dashboard = tableau.extensions.dashboardContent.dashboard;

      //  Try to init the extension
      urlQuery();

      //  Add event handler to watch for settings changes
      tableau.extensions.settings.addEventListener(tableau.TableauEventType.SettingsChanged, urlQuery);
    });
  });

  //  Function to configure the extension's properties
  function configure() {
    
    //  Where do we find the popup dialog?
    const popupUrl = links.configDialog;

    //  Display the dialog box
    tableau.extensions.ui.displayDialogAsync(popupUrl, '5', { height: 500, width: 500 }).then((closePayload) => {
     
     // Check to see if there's a new URL for the iframe
     urlQuery();

    }).catch((error) => {

        //  Write any errors to the console
        console.error(error.message);
    });
  }

  //  Function to query for the URL field
  function urlQuery() {

    //  Get the current selections for datasource/field
    let selections = tableau.extensions.settings.getAll();

    //  Check for the selections
    let worksheetId = selections[worksheetSettingsKey],
        fieldId = selections[fieldSettingsKey];

    //  Make sure the settings are defined
    let isValid =  (worksheetId && fieldId);
    if (isValid) {

      //  Find the worksheet
      let worksheet = tableau.extensions.dashboardContent.dashboard.worksheets.find( function(w) { 
        return w.name === selections.selectedWorksheet; 
      });
      
      //  Find the datasource
      worksheet.getSummaryDataAsync().then(function(response){

        //  Look for the desired column
        let columnIndex = null;
        for (let i=0; i< response.columns.length; i++){
          if (response.columns[i].fieldName === selections.selectedField) {
            columnIndex = response.columns[i].index;
          }
        }

        //  Make sure the column was found
        if (typeof columnIndex !== "null") {

          //  Column found, get the first URL in the data set
          let newUrl = response.data[0][columnIndex].value;

          //  Update the iframe
          updateIframe(newUrl);
        } else {

          //  Column not found anymore
          updateIframe(links.notFound);
        }
      })
    } else {

      //  Not configured properly, show the config image
      updateIframe(links.notFound);
    }
  }

  //  Function to calculate the new URL for the iframe, and update the DOM
  function updateIframe(newUrl) {

    // Get a reference to the iframe
    let myIframe = $('iframe#myIframe');

    //  Update the height, based on the container
    myIframe.css('height', $(window).height() - 5);
    myIframe.css('overflow','hidden');

    // Update the iframe in the UI
    $('iframe#myIframe').attr('src',newUrl);
  }
})();
