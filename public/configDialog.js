'use strict';

// Wrap everything in an anonymous function to avoid polluting the global namespace
(function () {
  
  //  Define the keys used to save settings within the extension
  const worksheetSettingsKey = 'selectedWorksheet';
  const fieldSettingsKey = 'selectedField';

  //  Init variables used to store the user's selections in the popup
  let selectedWorksheet;
  let selectedField;
  let dataModel = {};

  //  Define the selectors for HTML elements
  const worksheetsId = '#worksheets';
  const fieldsId = '#fields';
  const closeButtonId = '#closeButton';

  //  Wait for the popup's DOM to finish loading
  $(document).ready(function () {
    
    //  Initialize the dialog popup
    tableau.extensions.initializeDialogAsync().then(function (openPayload) {

      selectedWorksheet = tableau.extensions.settings.get(worksheetSettingsKey);
      selectedField = tableau.extensions.settings.get(fieldSettingsKey);
      console.log(selectedWorksheet + ": " + selectedField);

      //  Event handler for the close button and data source dropdown
      $(closeButtonId).click(closeDialog);
      $(worksheetsId).on('change',worksheetPicked);
      $(fieldsId).on('change', fieldPicked);

      //  Get a reference to the dashboard and it's data sources
      let dashboard = tableau.extensions.dashboardContent.dashboard;
      let visibleDatasources = [];

      //  Create a list of promises to run
      let completeCount = 0;
      let maxCount = dashboard.worksheets.length;

      // Loop through each worksheet
      dashboard.worksheets.forEach(function (worksheet) {

        //  Is this a new worksheet?
        if (!dataModel[worksheet.name]) {

          //  Save a reference to it
          dataModel[worksheet.name] = {};

          //  Is this the selected worksheet?
          let isSelected = typeof selectedWorksheet !== "undefined";

          //  Add to the dropdown memu
          addMenuItem({ id: worksheet.name, name: worksheet.name, selected:isSelected}, worksheetsId);
        }
        
        //  Get the data sources of that worksheet
        worksheet.getSummaryDataAsync().then(function (response) {

          //  Loop through each column in this worksheet
          response.columns.forEach(function(column){

            //  Only show string fields, because a url cannot be a date/number
            if (column.dataType == "string") {

              //  Check to see if this column is the selected one
              column.selected = (column.fieldName == selectedField);

              //  Save a reference to the column
              dataModel[worksheet.name][column.index] = column.fieldName;
            }
          })

          //  Increment the counter
          completeCount += 1;
          if (completeCount === maxCount) {
            //  Done fetching data, init the menus
            selectedWorksheet = $(worksheetsId).val();
            buildFieldMenu()
          }
        });
      });
    });
  });

  //  Helper function to create the dropdown menu item
  function addMenuItem(data, selector) {

    //  Create HTML for the data source menu item
    var myOption = $('<option></option>');
    myOption.attr('value',data.id);
    myOption.text(data.name);
    if (data.selected){
      myOption.attr('selected','selected');
    }

    //  Add to the dom
    $(selector).append(myOption);
  }

  //  Helper function to init the fields dropdown menu
  function buildFieldMenu(){

    //  clear any old options
    $(fieldsId).empty();

    //  Add the new options for just this data source
    for (let key in dataModel[selectedWorksheet]){

      //  Get a reference to the column
      let column = dataModel[selectedWorksheet][key];

      //  Create the data object
      let field = {
        id: column,
        name: column,
        selected: (column == selectedField)
      }

      //  Add to the UI
      addMenuItem(field, fieldsId)
    }
  }

  //  Function to uppdate the list of available fields, after the user picks a data source
  function worksheetPicked(event, args){

    //  Set the new data source
    selectedWorksheet = $(this).val();

    //  Build the fields menu
    buildFieldMenu();
  }

  //  Function to update the selected field, on user selection
  function fieldPicked(event, args){

    //  Save the selection
    selectedField = $(this).val();
  }

  //  Function to save the user's selections, and close the dialog box
  function closeDialog() {

    //  Get a reference to the extension's settings
    let currentSettings = tableau.extensions.settings.getAll();

    //  Use the setters to save the selections
    tableau.extensions.settings.set(worksheetSettingsKey, selectedWorksheet);
    tableau.extensions.settings.set(fieldSettingsKey, selectedField);

    //  Save the state, and close the dialog box
    tableau.extensions.settings.saveAsync().then((newSavedSettings) => {
      tableau.extensions.ui.closeDialog();
    });
  }
})();
