# DynamicIframe Extension

## Deployment

The code behind all Tableau extensions must be hosted by a web server.  To simplify deployment for this extension, the code is being hosted in GitLab at the following link:
```
https://tableau-extensions.gitlab.io/dynamiciframe   
```

This means you can just [click here](public/DynamicIframe.trex) to get to the *.trex* file's page.  Then click the download button to get the file.

![Download Button](screenshots/download-screen.png)

Once you have the file, you can add it to your dashboard by using the Extension button in Tableau Desktop or Tableau Server/Online.

![Dashboard Menu](screenshots/DashboardMenu.png)

If you wish to modify the extension, you can download the full source code under the public directory.  Make whatever changes are desired, and the host the website on your own web server.  If changes are made, make sure to update the *.trex* file's source-location.url attribute to point to your web server.  You will also need to update *main.js*, and update the *links.configDialog* property on line 13 to make sure the popup dialog can be found.

## Usage

In order to use this extension, you will need a field in your dataset that contains a URL.  Sometimes this needs to be created, as a calculated field in Tableau.  

### Step 1: Create a Viz
Create or modify any viz that will appear in the dashboard, to include your URL field.  An easy way to do this, is to just add the URL field as a detail within the Viz.  This will make sure Tableau gets the value for your URL as part of the Viz's query, but it won't affect the display of the Viz.

![Viz Detail](screenshots/viz-details.png)

### Step 2: Add the Extension
Make sure the Viz with the URL field has been added to your dashboard.  Now drag a new Extension box into the dashboard.  This will prompt you to select an extension.  Select **DynamicIframe.trex** as the extension to use, and you should see a page not found message.  This is expected, as we haven't told the extension which field to use to get a URL.  Click on the settings menu of the extension, and click **configure**.

![Configure Popup](screenshots/config.png)

First select the Viz that contains your URL field (since the dashboard can have many Vizs), and then select the field to use for your URL.  Click save, and you should see the extension update.  Keep in mind that if the dataset for the Viz contains several URLs, this extension will pick the first one it finds and display that.  

### Step 3: Filter Actions (optional)
A common use case for this extension would be using this in conjunction with a [Filter Action](https://onlinehelp.tableau.com/current/pro/desktop/en-us/actions_filter.htm).  So for example, you may start off with a dashboard showing a list of customers/products/locations/youtube video/etc and when you select one, you drill down to a secondary dashboard for the details.  If you can provide a link specific to that list, then you can show the contents of that link within the iframe of this extension.  

![Usage Example](screenshots/usage.gif)

## References/Notes
* More information regarding the Tableau Extensions API can be found [here](https://tableau.github.io/extensions-api/docs/trex_getstarted.html)
* This extension was built using version 1.0.0 of the Tableau Extension API
* The website in the screenshot showing information about zip codes can be found [here](https://www.unitedstateszipcodes.org/).  To use it, just create a calculated field with the following formula:

```    
'https://www.unitedstateszipcodes.org/' + myZipcodeField + '/'
```
